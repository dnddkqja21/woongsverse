# Metaverse Portfolio

## - Name
>**Woongsverse**

## - Description
>Unity Editor ver    
**2021.3.15f1**

>Rendering Pipeline     
**URP**

>Multiplay Server sdk   
**[photon engine](https://www.photonengine.com/)**

>Backend Server sdk     
**[backend.io](https://www.thebackend.io/)**

>Webview sdk        
**[vuplex.com](https://www.vuplex.com/)**

>WebRTC sdk     
**[agora.io](https://www.agora.io/)**


## - Progress
- [x] Editor Editor initial settings
- [x] Editor customization
- [x] Multiplayer Game
- [x] Building
- [x] 3rd Person Camera
- [x] UI 
- [x] Interaction
- [x] WebView
- [x] Scene 
- [x] 3D World
- [x] Light & Shadow
- [x] Post Processing
- [x] Video Chatting with WebRTC


## - ScreenShot
![ex_screenshot](./Screenshot/1st.png)

















