using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TEST : MonoBehaviour
{
    Animator animator;
    [SerializeField]
    GameObject laserPrefab;
    [SerializeField]
    Transform area;

    void Start()
    {
        if(SceneManager.GetActiveScene().buildIndex == (int)SceneList.World)
        {
            Invoke("NewMethod", 2f);
        }
        else if(SceneManager.GetActiveScene().buildIndex == (2))
        {
            animator = GameObject.Find("0").GetComponent<Animator>();
        }
    }

    private void NewMethod()
    {
        animator = PhotonManager.Instance.player.GetComponent<Animator>();
    }

    // test
    void Update()
    {        
         if(Input.GetKeyDown(KeyCode.Space))
        {
            GetRandomPosInBounds();
        }     
    }

    void GetRandomPosInBounds()
    {
        // 오브젝트의 바운딩 박스(Bounding Box) 정보 가져오기
        Bounds objectBounds = area.GetComponent<Renderer>().bounds;

        // x와 z의 범위 내에서 무작위 좌표 생성
        Vector3 randomPosition = new Vector3(
            Random.Range(area.position.x - objectBounds.extents.x, area.position.x + objectBounds.extents.x),
            0f,  // y는 일정한 값으로 유지 (오브젝트가 위아래로 이동하지 않는다고 가정)
            Random.Range(area.position.z - objectBounds.extents.z, area.position.z + objectBounds.extents.z)
        );

        // 무작위 좌표를 필요한 대로 사용합니다 (예: 해당 위치에 오브젝트를 생성)
        InstantiateYourObject(randomPosition);
    }
    void InstantiateYourObject(Vector3 position)
    {
        // 오브젝트를 생성하거나 무작위 위치로 어떤 작업을 수행합니다
        // 예를 들어, 무작위 위치에 큐브를 생성합니다
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = position;
    }
}
