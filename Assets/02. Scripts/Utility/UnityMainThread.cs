using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UnityEngine.UnityException: IsPersistent can only be called from the main thread. (오류 해결 방법)
/// </summary>

public class UnityMainThread : MonoBehaviour
{
    internal static UnityMainThread work;
    Queue<Action> jobs = new Queue<Action>();

    void Awake()
    {
        work = this;
    }

    void Update()
    {
        while (jobs.Count > 0)
            jobs.Dequeue().Invoke();
    }

    internal void AddJob(Action newJob)
    {
        jobs.Enqueue(newJob);
    }
}
