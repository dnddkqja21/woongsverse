using System;
using UnityEngine;

public static class DebugCustom
{
    // 디파인이 정의되지 않은 경우 컴파일에서 제외함
    //[System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void Log(object message)
    {
        // 에디터와 개발 빌드일 때만 로그를 컴파일
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.Log(message);
#endif
    }

    
    public static void Log(object message, UnityEngine.Object context)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.Log(message, context);
#endif
    }

    
    public static void LogWarning(object message)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.LogWarning(message);
#endif
    }

    
    public static void LogWarning(object message, UnityEngine.Object context)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.LogWarning(message, context);
#endif
    }

    
    public static void LogError(object message)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.LogError(message);
#endif
    }

    
    public static void LogError(object message, UnityEngine.Object context)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.LogError(message, context);
#endif
    }

    
    public static void LogFormat(string message, params object[] args)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.LogFormat(message, args);
#endif
    }

    
    public static void LogErrorFormat(string message, params object[] args)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.LogErrorFormat(message, args);
#endif
    }

    
    public static void DrawLine(Vector3 start, Vector3 end, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.DrawLine(start, end, color, duration, depthTest);
#endif
    }

    
    public static void DrawRay(Vector3 start, Vector3 dir, Color color = default(Color), float duration = 0.0f, bool depthTest = true)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        Debug.DrawRay(start, dir, color, duration, depthTest);
#endif
    }

    
    public static void Assert(bool condition)
    {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
        if (!condition) throw new Exception();
#endif
    }    
}
